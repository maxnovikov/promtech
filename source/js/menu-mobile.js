let header = $('.header');
let body = $('body');
let mobileToggler = $('.js-mobile-toggler');
let mobileCatalog = $('.menu-mobile__catalog-content');
let mobileCatalogToggler = $('.js-mobile-catalog-toggler');
let mobileCatalogHeight = mobileCatalog.height();
let mobileLangButton = $('.js-lang-menu .active');

mobileCatalog.css('max-height', '0');

mobileToggler.click(function(e) {
    e.preventDefault();

    if($('.catalog-open').length) {
        mobileCatalogToggler.trigger('click');
    }

    if($('.js-lang-menu.open').length) {
        mobileLangButton.trigger('click');
    }

    header.toggleClass('header-menu-mobile-open');

    setTimeout(function() {
        body.toggleClass('mobile-header-open');
    },800);
});

mobileLangButton.click(function() {
    $(this).parent().toggleClass('open');
});

mobileCatalogToggler.click(function(e) {
    e.preventDefault();

    let parentEl = $(this).parent();
    console.log($(this), parentEl);

    if(!parentEl.hasClass('catalog-open')) {
        parentEl.addClass('catalog-open');
        mobileCatalog.css('max-height', mobileCatalogHeight + 'px');
    } else {
        parentEl.removeClass('catalog-open');
        mobileCatalog.css('max-height', '0');
    }
});