const $deliveryOption = $('.js-delivery-option');
const $paymentOption = $('.js-payment-option');
const $selectedOption = $('.js-selected-option');
const $deliveryOptionContent = $('.js-delivery-option-content');
const $paymentOptionContent = $('.js-payment-option-content');
$deliveryOption.on('click', selectDeliveryOption);
$paymentOption.on('click', selectPaymentOption);

function selectDeliveryOption(e) {
  e.preventDefault();
  $deliveryOptionContent.removeClass('is-visible');

  selectOption(e);
}

function selectPaymentOption(e) {
  e.preventDefault();
  const $currentElement = $(e.currentTarget);

  $currentElement.closest('.js-accordion').accordion({active: "false"});

  $paymentOptionContent.removeClass('is-visible');
  selectOption(e);
}

function selectOption(e) {
  const $currentElement = $(e.currentTarget);
  const $optionName = $currentElement.find('a').text();
  const $contentId = $currentElement.find('a').attr('href');

  $currentElement.siblings().removeClass('is-selected');
  $currentElement.addClass('is-selected');
  $currentElement.closest('.js-accordion').next().text($optionName);
  console.log($currentElement);
  $($contentId).addClass('is-visible');
  $currentElement.closest('.js-accordion').accordion({active: "false"});
}
