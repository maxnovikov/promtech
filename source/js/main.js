@import '../../bower_components/jquery/dist/jquery.js'
@import '../../bower_components/jquery-ui/jquery-ui.js'
@import '../../bower_components/slick-carousel/slick/slick.js'
@import '../../bower_components/lightbox2/dist/js/lightbox.js'
@import 'vendor/amazonmenu.js'


$(function() {
    @import 'sliders.js'
    @import 'colorboxes.js'
    @import 'common.js'
    @import 'delivery-options.js'
    @import 'password.js'
    @import 'product-counter.js'
    @import 'menu-mobile.js'
    @import 'modal.js'
    @import 'catalog-filters.js'
    @import 'contact-tabs.js'
    @import 'header-hidding.js'
    @import 'autocomplete.js'
});