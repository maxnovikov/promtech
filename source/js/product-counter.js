const $productQuantity = $('.js-product-quantity');
const $addCount = $('.js-add');
const $reduceCount = $('.js-reduce');

$addCount.on('click', addCount);
$reduceCount.on('click', reduceCount);

let $productCurrentValue = parseInt($productQuantity.val());

function addCount(e) {
  e.preventDefault();
  let newValue = ( ++ $productCurrentValue );
  $(this).siblings($productQuantity).val(newValue);
}

function reduceCount(e) {
  e.preventDefault();
  if ($productCurrentValue > 0) {
    let newValue = ( -- $productCurrentValue );
    $(this).siblings($productQuantity).val(newValue);
  }
}
