const filtersShowButton = $('.js-filters-button');
const filtersCloseButton = $('.js-filters-close');

$('.js-dropdown-button').click(function(e) {
    e.preventDefault();
    let thisSiblingDropdownList = $(this).siblings('ul');
    let parentBlock = $(this).parent();

    parentBlock.toggleClass('dropdown-open');

    if(parentBlock.hasClass('dropdown-open')) {
        let maxHeight = thisSiblingDropdownList[0].scrollHeight;
        thisSiblingDropdownList.css('max-height', maxHeight + 'px');
    } else {
        thisSiblingDropdownList.css('max-height', '');
    }
});

$('.js-sorting-button').click(function() {
    let parent = $(this).parent();
    parent.toggleClass('open');
});

filtersShowButton.click(function (e) {
    e.preventDefault();
    $('body').toggleClass('filters-shown');
});

filtersCloseButton.click(function(e) {
    e.preventDefault();
    filtersShowButton.trigger('click');
});