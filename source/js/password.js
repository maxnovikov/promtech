const $passwordInput = $('.js-password');
const $passwordToggler = $('.js-password-show');

$passwordToggler.on('click', showPassword);

function showPassword(e) {
  let $passwordInputToChange = $(e.currentTarget).prev();

  if ($passwordInputToChange.attr("type") === "password") {
    $(e.currentTarget).prev().attr("type", "text");
  } else {
    $(e.currentTarget).prev().attr("type", "password");
  }
}
