const itemsJson = 'http://localhost:9999/search-results.json';
let items = [];

//url, image_url, code, price, value
$( "#desktop-autocomplete" ).autocomplete({
    appendTo: '.header__search',
    source: function( request, response ) {
        $.ajax( {
            url: "http://localhost:9999/search-results.json",
            dataType: "json",
            data: {
                term: request.term
            },
            success: function( data ) {
                let newData = [];

                for( let i= 0; i < data.length; i++ ) {

                    let requestedValue = request.term.toUpperCase();
                    let dataValue = data[i].value[0].toUpperCase();
                    let check = dataValue.indexOf( requestedValue );

                    if( check >= 0) {
                        newData.push(data[i]);
                    }
                }

                if(newData.length >= 4) {
                    newData = newData.slice(0,3); //max count of items == 3
                }

                response( newData );
            },
        });
    },
    minLength: 2,
}).autocomplete( "instance" )._renderItem = function(ul, item) {
    let checkItemsLength = $('.header__search .ui-menu>li').length >= 2;
    let template = "<a href='"+ item.url +"'>"
        + "<img src='" + item.image_url +"'>"
        + "<div class='autocomplete-item-name'>"
        + "<p>" + item.value[0] +"</p>"
        + "<strong>Код: " + item.code[0] + "</strong>"
        + "</div>"
        + "<p class='autocomplete-item-price'>" + item.price +"</p>"
        + "</a>";

    if(!checkItemsLength) {
        return $( "<li>" )
            .append( template )
            .appendTo( ul );
    } else {

        let button = '<button type="submit" class="desktop-autocomplete-submit-button">показать ещё</button>';

        return $( "<li>" )
            .append(template)
            .append( button )
            .appendTo( ul );
    }
};



$( "#mobile-autocomplete" ).autocomplete({
    appendTo: '.header__mobile-search-autocomplete',
    source: function( request, response ) {
        $.ajax( {
            url: "http://localhost:9999/search-results.json",
            dataType: "json",
            data: {
                term: request.term
            },
            success: function( data ) {
                let newData = [];

                for( let i= 0; i < data.length; i++ ) {

                    let requestedValue = request.term.toUpperCase();
                    let dataValue = data[i].value[0].toUpperCase();
                    let check = dataValue.indexOf( requestedValue );

                    if( check >= 0) {
                        newData.push(data[i]);
                    }
                }

                response( newData );
            }
        } );
    },
    minLength: 2
} ).autocomplete( "instance" )._renderItem = function( ul, item ) {
    let template = "<a href='"+ item.url +"'>"
        + "<div class='autocomplete-item-name'>"
        + "<p>" + item.value[0] +"</p>"
        + "</div>"
        + "</a>";

    return $( "<li>" )
        .append( template )
        .appendTo( ul );
};


$('.js-mobile-search-button').click(function(e) {
    e.preventDefault();

    $('body').addClass('autocomplete-shown');
});

$('.js-autocomplete-close').click(function(e) {
   e.preventDefault();

   $('body').removeClass('autocomplete-shown');
});