$('.js-footer-slider').slick({
    autoplay: true,
    slidesToShow: 7,
    autoplaySpeed: 500,
    speed: 4000,
    cssEase: 'linear',
    arrows: false,
    swipe: false,
    responsive: [
        {
            breakpoint: 991,
            settings: {
                slidesToShow: 6
            }
        },
        {
            breakpoint: 767,
            settings: "unslick"
        }
    ]
});

$('.js-partner-logotypes').slick({
    autoplay: true,
    slidesToShow: 6,
    autoplaySpeed: 500,
    speed: 4000,
    cssEase: 'linear',
    arrows: false,
    swipe: false,
});

$('.js-banner-slider').slick({
    fade: true,
    autoplay: true,
    slidesToShow: 1,
    swipe: false,
    responsive: [
        {
            breakpoint: 767,
            settings: {
                arrows: false
            }
        }
    ]
});

if($(document).width() < 768) {
    $('.js-mobile-product-gallery').slick({
        arrows: false,
        dots: true
    });
}

if($(document).width() < 992) {
    $('.js-product-rows-slider').slick({
        slidesToShow: 3,
        arrows: false,
        responsive: [
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 2
                }
            }
        ]
    });
}