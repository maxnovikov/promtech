$('.js-tabs').tabs({
    // hide: {effect: "fade", duration: 400},
    show: {effect: "fade", duration: 400},
  }
);

$('.js-accordion').accordion({
    collapsible: true,
    heightStyle: "content",
});

amazonmenu.init({
    menuid: 'mysidebarmenu'
});

$('#mysidebarmenu')
    .on('mouseover', function() {
        $('main').addClass('hover-menu');
    })
    .on('mouseleave', function() {
        $('main').removeClass('hover-menu');
    });

$('.header__catalog')
    .on('mouseover', function() {
        $('body').addClass('menu-visible');
    })
    .on('mouseleave', function() {
        $('body').removeClass('menu-visible');
    });

setIndexBannerSlideWidth('.index__banner-slide-img');
setIndexBannerSlideWidth('.contacts__right-side');

function setIndexBannerSlideWidth(element) {
    if($(document).width() > 991 && $(element).length) {
        let setWidth = $('.header__wrapper-right-side').width();

        $(element).css('min-width', setWidth + 'px')
            .css('max-width', setWidth + 'px');
    } else {
        $(element).css('min-width', '')
            .css('max-width', '');
    }
}

$(window).on('resize', function() {
    setIndexBannerSlideWidth('.index__banner-slide-img');
    setIndexBannerSlideWidth('.contacts__right-side');
});

if ($(window).width() < 767) {
  $('.js-account').on('click', function (e) {
    e.preventDefault();
    $('.js-tabs-list').toggleClass('is-visible');
    $('.js-title').toggleClass('is-fixed');
    $('body').addClass('modal-open')
  });

  $('.js-tab-link').on('click', function() {
    $('.js-tabs-list').removeClass('is-visible');
    $('.js-title').removeClass('is-fixed');
    $('body').removeClass('modal-open')
  });
}
