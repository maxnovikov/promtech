function changeTab() {
    if($('.contacts__region-tabs').length) {
        let selectContactRegion = $('#select-region');
        let selectedContactRegionIndex = selectContactRegion[0].selectedIndex;
        let selectedContactValue = selectContactRegion[0].options[selectedContactRegionIndex].value;
        let contactTab = $('.contacts__region-tabs-tab');
        let selectedContactTab = $('.contacts__region-tabs-tab.' + selectedContactValue);

        $(selectedContactTab).addClass('active');
        $(contactTab).not('.contacts__region-tabs-tab.' + selectedContactValue).removeClass('active');
    }
}

changeTab();


$('#select-region').on('change', function() {
   changeTab();
});