function openModal(modalId) {
    $(modalId).addClass('active');
    $('body').addClass('modal-open');
    $(modalId).addClass('active');

    setTimeout(function() {
        $(modalId).addClass('active-visible');
    }, 100);
}

function closeModal() {
    let openModal = $('.modal__container.active');

    $(openModal).removeClass('active-visible');

    setTimeout(function() {
        $(openModal).removeClass('active');
        $('body').removeClass('modal-open');
    }, 100);
}

$('.js-modal-open').click(function(e) {
    e.preventDefault();
    let currentModal = $(this).attr('href');
    console.log(currentModal);

    openModal(currentModal);
});

$('.js-modal-close').click(function(e) {
    e.preventDefault();

    closeModal();
});

$(document).on('mousedown', function(e) {
    let modalActive = $('.modal__container.active');

    if(modalActive.length && modalActive.has(e.target).length === 0) {
        closeModal();
    }
});